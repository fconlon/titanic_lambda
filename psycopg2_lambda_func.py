from boto3.session import Session
from json import loads
import psycopg2

# get aws secret
client = Session().client(
        service_name='secretsmanager',
        region_name='us-east-1'
    )

secret_dict = loads(
        client.get_secret_value(
            SecretId='TitanicDatabaseUser'
        )['SecretString']
    )
    
conn = psycopg2.connect(
        host=secret_dict['host'],
        user=secret_dict['username'],
        password=secret_dict['password'],
        database='titanic'
    )

def lambda_handler(event, context):
    cur = conn.cursor()
    cur.execute('SELECT * FROM passengers;')
    passengers = cur.fetchall()
    cur.close()
    return passengers #{'test': 'testing'}
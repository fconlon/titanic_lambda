from boto3.session import Session
from json import loads
from sqlalchemy import URL, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

# get aws secret
client = Session().client(
        service_name='secretsmanager',
        region_name='us-east-1'
    )

secret_dict = loads(
        client.get_secret_value(
            SecretId='TitanicDatabaseUser'
        )['SecretString']
    )

# set up sqlalchemy
database_uri = URL.create(
        drivername='postgresql',
        username=secret_dict['username'],
        password=secret_dict['password'],
        host=secret_dict['host'],
        port=secret_dict['port'],
        database='titanic',
    )
Base = declarative_base()
engine = create_engine(database_uri)
Base.metadata.bind = engine
db_session = scoped_session(sessionmaker())(bind=engine)

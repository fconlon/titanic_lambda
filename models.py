from sqlalchemy import  Column, Integer, Boolean, String

from db_utils import Base

class Passenger(Base):
    __tablename__ = 'passengers'

    id = Column(Integer(), primary_key=True)
    passenger_class = Column(Integer(), nullable=False)
    survived = Column(Boolean(), nullable=False)
    name = Column(String(), nullable=False)
    gender = Column(String(), nullable=False)
    age = Column(Integer(), nullable=True)